/// <reference path='../lib/definitions/phaser.d.ts'/>
/// <reference path='SiegeGame.ts'/>
/// <reference path='Player.ts'/>

module Siege {
    export class GUI {
        constructor() {
            this.group = new Phaser.Group(SiegeGame.game);

            this.printName(SiegeGame.player);
            this.printName(SiegeGame.player.enemyPlayer);

            this.playerHealthText = this.createHealth(SiegeGame.player);
            this.enemyPlayerHealthText = this.createHealth(SiegeGame.player.enemyPlayer);

            this.playerGoldText = this.createCurrency(SiegeGame.player);

            SiegeGame.game.world.bringToTop(this.group)
        }

        public group: Phaser.Group;

        private playerHealthText: Phaser.Text;
        private enemyPlayerHealthText: Phaser.Text;
        private playerGoldText: Phaser.Text;

        public update(): void {
            this.playerHealthText.text = SiegeGame.player.base.health.toString();
            this.enemyPlayerHealthText.text = SiegeGame.player.enemyPlayer.base.health.toString();

            this.playerGoldText.text = 'Gold: ' + SiegeGame.player.gold.toString();
            var offset: number;
            switch (SiegeGame.player.position) {
                case Position.Left:
                    offset = 0;
                    break;
                case Position.Right:
                    offset = 1;
                    break;
            }
            this.playerGoldText.reset((SiegeGame.game.width - this.playerGoldText.width) * offset, 0);

            SiegeGame.game.world.bringToTop(this.group)
        }

        public createCharButton(charType: CharacterType, player: Player, y: number) {
            var character: Character = Character.getCharacterFromType(charType, player);
            var frameName: string = character.assetName;
            var offset: number = player.position == Position.Left ? 0 : 1;

            var button = SiegeGame.game.add.button(0, 0, 'characters-icons');
            button.frameName = frameName;
            //button.anchor.set(0.5);
            button.width = SiegeGame.game.width / 25;
            button.height = SiegeGame.game.width / 25;
            button.reset((SiegeGame.game.width - button.width) * offset, y);

            button.onInputDown.add(() => {
                SiegeGame.socket.emit('enemy-character-created', {
                    id: SiegeGame.player.enemyPlayer.id,
                    characterType: charType
                });

                Character.create(charType, player)
            })

            this.group.add(button.animations.sprite);
        }

        private printName(player: Player): void {
            var nameTextStyle = { fill: '#FFF' };
            var nameText = SiegeGame.game.add.text(player.base.baseSprite.x + player.base.baseSprite.width / 2, SiegeGame.game.height / 2.8, player.name.toString(), nameTextStyle, this.group);

            nameText.anchor.setTo(0.5);
            nameText.font = 'Arial';
            nameText.fontSize = 20;
            nameText.stroke = '#000';
            nameText.strokeThickness = 3;
        }

        private createHealth(player: Player): Phaser.Text {
            var healthTextStyle = { fill: '#0DFF21' };
            var text = SiegeGame.game.add.text(player.base.baseSprite.x + player.base.baseSprite.width / 2,
                SiegeGame.game.height / 2.5, player.base.health.toString(), healthTextStyle, this.group);

            text.anchor.setTo(0.5);
            text.font = 'Arial';
            text.fontSize = 17;
            text.stroke = '#000';
            text.strokeThickness = 3;

            return text;
        }

        private createCurrency(player: Player): Phaser.Text {
            var currencyTextStyle = { fill: '#FFF936' };
            var offset;
            switch (player.position) {
                case Position.Left:
                    offset = 0;
                    break;
                case Position.Right:
                    offset = 1;
                    break;
            }
            var text = SiegeGame.game.add.text(0,
                0, 'Gold: ' + player.gold.toString(), currencyTextStyle, this.group);

            text.font = 'Arial';
            text.fontSize = 25;
            // text.fontStyle.bold();
            text.stroke = '#000';
            text.strokeThickness = 3;

            return text;
        }
    }
}